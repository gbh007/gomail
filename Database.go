package gomail

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq" //import pg driver
)

// database структура для БД
type database struct {
	db *sql.DB
}

func (db *database) connect() error {
	dbn, err := sql.Open(
		"postgres",
		fmt.Sprintf(
			"user=%s password=%s dbname=%s host=%s port=%d sslmode=disable",
			dbConfig.GetUser(),
			dbConfig.GetPassword(),
			dbConfig.GetDBName(),
			dbConfig.GetHost(),
			dbConfig.GetPort(),
		))
	if err == nil {
		db.db = dbn
	} else {
		log.Error("can't connect database", err)
		return err
	}
	if err = db.db.Ping(); err != nil {
		log.Error("databse not ping", err)
		return err
	}
	return nil
}
