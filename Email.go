package gomail

import (
	"database/sql"
)

// Email структура для отображения таблицы конфигурации email адресов
type Email struct {
	Code     string
	Login    string
	Pass     string
	Host     string
	Fullhost string
	Name     sql.NullString
}

// selectEmails получает все доступные адреса для отправки почты
func (db *database) selectEmails() ([]*Email, error) {
	res := make([]*Email, 0)
	rows, err := db.db.Query(`SELECT code, login, pass, host, fullhost, name FROM mail.emails`)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	for rows.Next() {
		email := &Email{}
		if err := rows.Scan(
			&email.Code,
			&email.Login,
			&email.Pass,
			&email.Host,
			&email.Fullhost,
			&email.Name,
		); err != nil {
			log.Error(err)
		} else {
			res = append(res, email)
		}
	}
	return res, nil
}

// selectEmailByCode получить адрес отправки по коду
func (db *database) selectEmailByCode(code string) (*Email, error) {
	email := &Email{}
	row := db.db.QueryRow(`SELECT code, login, pass, host, fullhost, name FROM mail.emails WHERE code = $1`, code)
	if err := row.Scan(
		&email.Code,
		&email.Login,
		&email.Pass,
		&email.Host,
		&email.Fullhost,
		&email.Name,
	); err != nil {
		log.Error(err)
		log.Info(code)
		return nil, err
	}
	return email, nil
}

// selectEmailByLogin получить адрес отправки по логину
func (db *database) selectEmailByLogin(login string) (*Email, error) {
	email := &Email{}
	row := db.db.QueryRow(`SELECT code, login, pass, host, fullhost, name FROM mail.emails WHERE login = $1`, login)
	if err := row.Scan(
		&email.Code,
		&email.Login,
		&email.Pass,
		&email.Host,
		&email.Fullhost,
		&email.Name,
	); err != nil {
		log.Error(err)
		log.Info(login)
		return nil, err
	}
	return email, nil
}
