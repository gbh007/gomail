package gomail

import (
	"bytes"
	"html/template"
	"net/smtp"
	"strings"
	"time"
)

func trimEmails(sl []string, lim int) (res []string, sln []string) {
	if len(sl) > lim {
		res = sl[:lim]
		sln = sl[lim:]
	} else {
		res = sl
	}
	return
}

// Mail структура для работы с почтой
type Mail struct {
	template *template.Template
	db       *database
}

// Delete помечает сообщение как не действительное
func (m *Mail) Delete(id int) error {
	mess, err := m.db.NewMessageSelectBuilder().SelectByID(id, true)
	if err != nil {
		return err
	}
	if mess.Valid == false {
		log.Error(errMessageIsNotValid)
		log.Info(id)
		return errMessageIsNotValid
	}
	if mess.Sended == true {
		log.Error(errMessageAlredySent)
		log.Info(id)
		return errMessageAlredySent
	}
	c, err := m.db.updateMessageValid(id, false)
	if err != nil {
		return err
	}
	if c > 0 {
		log.Info(id, "deleted")
	}
	return nil
}

// NewMessageSelectBuilder возврашает новый объект фильтра сообщений
func (m *Mail) NewMessageSelectBuilder() *MessageSelectBuilder { return m.db.NewMessageSelectBuilder() }

// Send отправляет сообщение с указаным ID
func (m *Mail) Send(id int) error {
	mess, err := m.db.NewMessageSelectBuilder().SelectByID(id, false)
	if err != nil {
		return err
	}
	if mess.Valid == false {
		log.Error(errMessageIsNotValid)
		log.Info(id)
		return errMessageIsNotValid
	}
	if mess.Sended == true {
		log.Error(errMessageAlredySent)
		log.Info(id)
		return errMessageAlredySent
	}
	email, err := m.db.selectEmailByLogin(mess.FromEmail)
	if err != nil {
		return err
	}

	// подготовка сообщения
	buff := &bytes.Buffer{}
	buff.WriteString("From : " + mess.FromEmail)
	buff.WriteString("\nTo : " + strings.Join(mess.ToEmails, ";"))
	buff.WriteString("\nSubject : " + mess.Subject + "\n")
	buff.WriteString("MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n")
	buff.WriteString(mess.Body)

	// отправка сообщения
	auth := smtp.PlainAuth("", email.Login, email.Pass, email.Host)
	to := make([]string, len(mess.ToEmails))
	copy(to, mess.ToEmails)
	for len(to) > 0 {
		var shortTo []string
		shortTo, to = trimEmails(to, mailConfig.GetMaxEmailToTime())
		err := smtp.SendMail(email.Fullhost, auth, email.Login, shortTo, buff.Bytes())
		if err != nil {
			log.Error(err)
			log.Info("failed to send", shortTo)
		} else {
			log.Info(mess.ID, "sent to", shortTo)
		}
	}

	// сохранение информации об отправке
	_, err = m.db.updateMessageSend(mess.ID)
	if err != nil {
		return err
	}
	return nil
}

// MakeMessage создает сообщение по шаблону
func (m *Mail) MakeMessage(fromCode, toCode, subject, templateName string, data interface{}, alterBody, info string) (*Message, error) {
	mess := &Message{
		Valid:   true,
		Subject: subject,
	}
	email, err := m.db.selectEmailByCode(fromCode)
	if err != nil {
		return nil, errSenderNotFound
	}
	mess.FromEmail = email.Login
	to, err := m.db.selectMailingListByCode(toCode)
	if err != nil {
		return nil, errMailingListNotFound
	}
	mess.ToEmails = to.Recipients
	if info != "" {
		mess.Info.String = info
		mess.Info.Valid = true
	}
	buff := &bytes.Buffer{}
	if templateName != "" {
		if err := m.template.ExecuteTemplate(buff, templateName, data); err != nil {
			log.Error(err)
			log.Info(fromCode, toCode, subject, templateName)
			return nil, err
		}
	} else {
		buff.WriteString(alterBody)
	}
	mess.Body = buff.String()
	mess.CreationTime.Time = time.Now().UTC()
	mess.CreationTime.Valid = true

	// добавление сообщения в базу
	if _, err := m.db.insertMessage(mess); err != nil {
		log.Info(fromCode, toCode, subject, templateName)
		return nil, err
	}
	log.Info("created", mess.ID)
	return mess, nil
}

// GetMessageByID возвращает письмо по его адрессу
func (m *Mail) GetMessageByID(id int) (*Message, error) {
	return m.db.NewMessageSelectBuilder().SelectByID(id, false)
}

// GetEmails возврашает список доступных почт для отправки
func (m *Mail) GetEmails() ([]*Email, error) {
	return m.db.selectEmails()
}

// GetMailingLists возврашает список доступных списков рассылки
func (m *Mail) GetMailingLists() ([]*MaillingList, error) {
	return m.db.selectMailingLists()
}

// SetToList обновляет список получателей сообщения
func (m *Mail) SetToList(id int, to []string) (int, error) {
	mess, err := m.db.NewMessageSelectBuilder().SelectByID(id, true)
	if err != nil {
		return 0, err
	}
	if mess.Valid == false {
		log.Error(errMessageIsNotValid)
		log.Info(id)
		return 0, errMessageIsNotValid
	}
	if mess.Sended == true {
		log.Error(errMessageAlredySent)
		log.Info(id)
		return 0, errMessageAlredySent
	}
	c, err := m.db.updateMessageToEmails(id, to)
	if c > 0 {
		log.Info(id, "mailling list update", to)
	}
	return c, err
}
