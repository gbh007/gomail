package gomail

import (
	"database/sql"
	"github.com/lib/pq"
)

// MaillingList структура для отображения таблицы адресов доставки
type MaillingList struct {
	Code       string
	Recipients []string
	Name       sql.NullString
}

// selectMailingLists получает все списки адресов доставки
func (db *database) selectMailingLists() ([]*MaillingList, error) {
	res := make([]*MaillingList, 0)
	rows, err := db.db.Query(`SELECT code, recipients, name FROM mail.mailing_lists`)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	for rows.Next() {
		ml := &MaillingList{}
		if err := rows.Scan(
			&ml.Code,
			pq.Array(&ml.Recipients),
			&ml.Name,
		); err != nil {
			log.Error(err)
		} else {
			res = append(res, ml)
		}
	}
	return res, nil
}

// selectMailingListByCode получает список доставки по коду
func (db *database) selectMailingListByCode(code string) (*MaillingList, error) {
	ml := &MaillingList{}
	row := db.db.QueryRow(`SELECT code, recipients, name FROM mail.mailing_lists WHERE code = $1`, code)
	if err := row.Scan(
		&ml.Code,
		pq.Array(&ml.Recipients),
		&ml.Name,
	); err != nil {
		log.Error(err)
		log.Info(code)
		return nil, err
	}
	return ml, nil
}
