package gomail

import (
	"database/sql"
	"github.com/lib/pq"
	"time"
)

// Message почтовое сообщение
type Message struct {
	ID           int
	Sended       bool
	Valid        bool
	CreationTime pq.NullTime
	SentTime     pq.NullTime
	FromEmail    string
	ToEmails     []string
	Subject      string
	Body         string
	Info         sql.NullString
}

// insertMessage добавляет новое сообщение
func (db *database) insertMessage(ms *Message) (int, error) {
	row := db.db.QueryRow(`INSERT INTO mail.messages (sended, valid, creation_time, sent_time, from_email, to_emails, subject, body, info) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id`,
		ms.Sended,
		ms.Valid,
		ms.CreationTime,
		ms.SentTime,
		ms.FromEmail,
		pq.Array(ms.ToEmails),
		ms.Subject,
		ms.Body,
		&ms.Info,
	)
	if err := row.Scan(&ms.ID); err != nil {
		log.Error(err)
		return 0, err
	}
	return ms.ID, nil
}

// updateMessageValid обновляет статус корректности сообщения
func (db *database) updateMessageValid(id int, valid bool) (int, error) {
	res, err := db.db.Exec(`UPDATE mail.messages
        SET
            valid = $1
		WHERE id = $2`,
		valid,
		id,
	)
	if err != nil {
		log.Error(err)
		log.Info(id)
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		log.Error(err)
	}
	return int(count), nil
}

// updateMessageSended обновляет время отправления сообщения сообщения
func (db *database) updateMessageSend(id int) (int, error) {
	res, err := db.db.Exec(`UPDATE mail.messages
        SET
            sended = $1,
            sent_time = $2
		WHERE id = $3`,
		true,
		time.Now().UTC(),
		id,
	)
	if err != nil {
		log.Error(err)
		log.Info(id)
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		log.Error(err)
	}
	return int(count), nil
}

// updateMessageToEmails обновляет список получателей сообщения
func (db *database) updateMessageToEmails(id int, to []string) (int, error) {
	res, err := db.db.Exec(`UPDATE mail.messages
        SET
			to_emails = $1
		WHERE id = $2`,
		pq.Array(to),
		id,
	)
	if err != nil {
		log.Error(err)
		log.Info(id)
		return 0, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		log.Error(err)
	}
	return int(count), nil
}
