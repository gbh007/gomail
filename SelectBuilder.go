package gomail

import (
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"time"
)

// MessageSelectBuilder структура для выполенния сложных запросов с фильтром к таблице сообщений
// не создавать без метода NewMessageSelectBuilder
type MessageSelectBuilder struct {
	Sent              int  // отправлено ли сообщение: -1 - любой 0 - неотправлено 1 - отправлено
	Valid             int  // действительно ли сообщение: -1 - любой 0 - не действительно 1 - действительно
	Short             bool // обрезать тело сообщение false - не обрезать тело сообщения true - тело сообщения пустая строка
	Order             int  // сортировка по полю -1 - без сортировки 0 - по ИД 1 - по времени создания письма 2 - по времени отправки письма 3 - по заголовку письма (тема)
	Reverse           bool // порядок данных false - прямой true - обратный
	MaxCount          int  // масимальное количество записей в ответе, игнорируется если меньше 1
	Offset            int  // пропустить первые записей, игнорируется если меньше 1
	db                *sql.DB
	baseSelect        string
	shortSelect       string
	baseWhere         string
	CreationStartTime *time.Time
	CreationEndTime   *time.Time
	SentStartTime     *time.Time
	SentEndTime       *time.Time
}

// NewMessageSelectBuilder возврашает новый объект фильтра сообщений
func (db *database) NewMessageSelectBuilder() *MessageSelectBuilder {
	return &MessageSelectBuilder{
		db:          db.db,
		Sent:        -1,
		Valid:       1,
		MaxCount:    20,
		Short:       true,
		baseSelect:  "SELECT id, sended, valid, creation_time, sent_time, from_email, to_emails, subject, body, info FROM mail.messages",
		shortSelect: `SELECT id, sended, valid, creation_time, sent_time, from_email, to_emails, subject, '' AS body, info FROM mail.messages`,
		baseWhere:   " WHERE id IS NOT NULL", // проверка на null не нужна это просто затычка для уменьшения количества кода на go
	}
}

func (mb *MessageSelectBuilder) addArg(args *[]interface{}, arg interface{}) string {
	*args = append(*args, arg)
	return fmt.Sprintf("$%d", len(*args))
}

func (mb *MessageSelectBuilder) makeWhere(args *[]interface{}) string {
	query := mb.baseWhere
	if mb.CreationStartTime != nil {
		query += " AND creation_time>=" + mb.addArg(args, mb.CreationStartTime)
	}
	if mb.CreationEndTime != nil {
		query += " AND creation_time<=" + mb.addArg(args, mb.CreationEndTime)
	}
	if mb.SentStartTime != nil {
		query += " AND sent_time>=" + mb.addArg(args, mb.SentStartTime)
	}
	if mb.SentEndTime != nil {
		query += " AND sent_time<=" + mb.addArg(args, mb.SentEndTime)
	}
	if mb.Sent == 0 {
		query += " AND sended = " + mb.addArg(args, false)
	}
	if mb.Sent == 1 {
		query += " AND sended = " + mb.addArg(args, true)
	}
	if mb.Valid == 0 {
		query += " AND valid = " + mb.addArg(args, false)
	}
	if mb.Valid == 1 {
		query += " AND valid = " + mb.addArg(args, true)
	}
	return query
}

func (mb *MessageSelectBuilder) makeOrder(args *[]interface{}) string {
	query := ""
	switch mb.Order {
	case 0:
		query += " ORDER BY id"
	case 1:
		query += " ORDER BY creation_time"
	case 2:
		query += " ORDER BY sent_time"
	case 3:
		query += " ORDER BY subject"
	}
	// поскольку менять порядок без сортировки нельзя то тут проверяется что сортировка применилась
	// для этого реализована проверка с пустой строкой,
	// возможно более правильным будет проверка на вхождение ключевого слова в строку
	// или на значение mb.Order (или вхождение его в диапазон),
	// но выбрана проверка на пустую строку из-за ее простоты :-)
	if query != "" && mb.Reverse {
		query += " DESC"
	}
	if mb.MaxCount > 0 {
		query += " LIMIT " + mb.addArg(args, mb.MaxCount)
	}
	if mb.Offset > 0 {
		query += " OFFSET " + mb.addArg(args, mb.Offset)
	}
	return query
}

// Select выполняет запрос с настроенными параметрами
func (mb *MessageSelectBuilder) Select() (res []*Message) {
	args := make([]interface{}, 0)
	var query = mb.baseSelect
	if mb.Short {
		query = mb.shortSelect
	}
	query += mb.makeWhere(&args) + mb.makeOrder(&args)
	rows, err := mb.db.Query(query, args...)
	if err != nil {
		log.Error(err)
		return
	}
	for rows.Next() {
		ms := &Message{}
		err = rows.Scan(
			&ms.ID,
			&ms.Sended,
			&ms.Valid,
			&ms.CreationTime,
			&ms.SentTime,
			&ms.FromEmail,
			pq.Array(&ms.ToEmails),
			&ms.Subject,
			&ms.Body,
			&ms.Info,
		)
		if err != nil {
			log.Error(err)
			continue
		}
		res = append(res, ms)
	}
	return
}

// SelectByID получает сообщение по его ID
func (mb *MessageSelectBuilder) SelectByID(id int, short bool) (*Message, error) {
	var query = mb.baseSelect
	if short {
		query = mb.shortSelect
	}
	query += " WHERE id = $1"
	row := mb.db.QueryRow(query, id)
	ms := &Message{}
	err := row.Scan(
		&ms.ID,
		&ms.Sended,
		&ms.Valid,
		&ms.CreationTime,
		&ms.SentTime,
		&ms.FromEmail,
		pq.Array(&ms.ToEmails),
		&ms.Subject,
		&ms.Body,
		&ms.Info,
	)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	return ms, nil
}
