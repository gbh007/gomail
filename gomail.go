package gomail

import (
	"fmt"
	"html/template"
	"sync"
)

var (
	errSenderNotFound      = fmt.Errorf("sender not found")
	errMailingListNotFound = fmt.Errorf("mailing list not found")
	errMessageIsNotValid   = fmt.Errorf("message is not valid")
	errMessageAlredySent   = fmt.Errorf("message alredy sent")
)

// IsSenderNotFound возвращает истину если ошибка это нет такого отправителя
func IsSenderNotFound(err error) bool { return err == errSenderNotFound }

// IsMailingListNotFound возвращает истину если ошибка это нет такого списка получателей
func IsMailingListNotFound(err error) bool { return err == errMailingListNotFound }

// IsMessageIsNotValid возвращает истину если ошибка это сообщение не действительно
func IsMessageIsNotValid(err error) bool { return err == errMessageIsNotValid }

// IsMessageAlredySent возвращает истину если ошибка это сообщение уже отправлено
func IsMessageAlredySent(err error) bool { return err == errMessageAlredySent }

// MailConfig конфигурация для почты
type MailConfig interface {
	GetMaxEmailToTime() int
	GetTemplateFilePattern() string
}

// DBConfig интерфейс для конфигурации пакета
type DBConfig interface {
	GetUser() string
	GetPassword() string
	GetDBName() string
	GetHost() string
	GetPort() int
}

// Logger интерфейс для логирования в пакете
type Logger interface {
	Debug(v ...interface{}) bool
	Info(v ...interface{}) bool
	Warning(v ...interface{}) bool
	Error(v ...interface{}) bool
	Critical(v ...interface{}) bool
	Panic(v ...interface{})
}

var _mail *Mail
var _once sync.Once
var dbConfig DBConfig
var mailConfig MailConfig
var log Logger

// GetMail возвращает текущий объект для работы с почтой
// вызовет панику если config и log не были установлены сетерами
func GetMail() *Mail {
	if log == nil {
		panic("log is nil")
	}
	if dbConfig == nil {
		log.Panic("db config is nil")
	}
	if mailConfig == nil {
		log.Panic("mail config is nil")
	}
	_once.Do(func() {
		// подключение к базе
		db := &database{}
		db.connect()
		// получение шаблонов почтовых сообщений
		tmpl, err := template.ParseGlob(mailConfig.GetTemplateFilePattern())
		if err != nil {
			log.Error(err)
		}
		if tmpl == nil {
			tmpl = template.New("error")
		}
		_mail = &Mail{db: db, template: tmpl}
	})
	return _mail
}

// SetLogger устанавливает логгер для пакета
func SetLogger(logger Logger) { log = logger }

// SetDBConfig устанавливает конфигурацию базы данных для пакета
func SetDBConfig(cnf DBConfig) { dbConfig = cnf }

// SetMailConfig устанавливает конфигурацию почты для пакета
func SetMailConfig(cnf MailConfig) { mailConfig = cnf }
