-- удаление старых таблиц
DROP TABLE IF EXISTS mail.emails;
DROP TABLE IF EXISTS mail.mailing_lists;
DROP TABLE IF EXISTS mail.messages;
DROP SCHEMA IF EXISTS mail;

-- создание схемы для почты
CREATE SCHEMA IF NOT EXISTS mail;

-- создание таблицы для конфигурации адресов отправки почты
CREATE TABLE mail.emails(
    code TEXT PRIMARY KEY,
    login TEXT NOT NULL,
    pass TEXT NOT NULL,
    host TEXT NOT NULL,
    fullhost TEXT NOT NULL,
    name TEXT
);

-- создание таблицы для конфигурации списков адресов получателей
CREATE TABLE mail.mailing_lists(
    code TEXT PRIMARY KEY,
    recipients TEXT ARRAY NOT NULL,
    name TEXT
);

-- создание таблицы для сообщений (как отправленых так и черновиков)
CREATE TABLE mail.messages(
    id SERIAL PRIMARY KEY,
    sended BOOLEAN NOT NULL DEFAULT FALSE,
    valid BOOLEAN NOT NULL DEFAULT TRUE,
    creation_time TIMESTAMP,
    sent_time TIMESTAMP,
    from_email TEXT NOT NULL,
    to_emails TEXT ARRAY NOT NULL,
    subject TEXT NOT NULL,
    body TEXT NOT NULL,
    info JSON NOT NULL
);
